/* 
	Nicholas Muenchen
   CPSC 1020 Section 1, Sp17
	nmuench
*/
#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <stdio.h>
#include <stdlib.h>

typedef struct header_t 
{
	char type[5];
	int width;
	int height;
	int maxVal;
} header_t;

typedef struct pixel_t
{
		unsigned char  red;
		unsigned char  green;
		unsigned char  blue;
} pixel_t;



void readHeader(header_t*, FILE*);
void readImage(header_t*, pixel_t **, FILE*);
void chooseTransform(header_t *, pixel_t **, FILE*);
void printP6Image(header_t*, pixel_t **, FILE*);
void grayScaleImage(header_t *, pixel_t **, FILE*);
void flipImage(header_t *, pixel_t **, FILE*);
void rotateLeft(header_t * , pixel_t **, FILE*);
void rotateRight(header_t *, pixel_t **, FILE*);
void color2Negative(header_t*, pixel_t **, FILE*);
void reprint(header_t*, pixel_t**, FILE*);
void verticalHalfMirror(header_t *, pixel_t **, FILE*);
void horizontalHalfMirror(header_t *, pixel_t **, FILE*);
#endif


