/* Nicholas Muenchen
   CPSC 1020 Section 1, Sp17
	nmuench
*/

#include "functions.h"
/*
	This functions reads int the header of the image 
	which the user wishes to transform.
*/
void readHeader(header_t *hdr, FILE* img)
{
	fscanf(img, "%s %d %d %d ", hdr->type, &hdr->width, &hdr->height, &hdr->maxVal);
}

/*
	This function reads in the data for the pixels of the
	image that will be transformed.
*/
void readImage(header_t *hdr, pixel_t **pixArr, FILE* img)
{
	int i;
	int j;
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
		fscanf(img, "%c%c%c", &pixArr[i][j].red, &pixArr[i][j].green, &pixArr[i][j].blue);
		}
	}
}

/*
	This functions explains to the user how to choose which transformation
	they would like to perform, accepts their input, makes sure that they 
	have selected a valid transformation, and then calls the appropriate function
	to perfrom the desired transformation
*/
void chooseTransform(header_t *hdr, pixel_t **pixelArray, FILE* saveImg)
{
	int userVal = 0;
	fprintf(stdout,"Please choose a trancformation from the list below: \n");
	fprintf(stdout,"1. Grayscale\n");
	fprintf(stdout,"2. Color to Negative\n");
	fprintf(stdout,"3. Flip the Image\n");
	fprintf(stdout,"4. Rotate Right\n");
	fprintf(stdout,"5. Rotate Left\n");
	fprintf(stdout,"6. Reprint the Image\n");
	fprintf(stdout,"7. Horizontal Half Mirror\n");
	fprintf(stdout,"8. Vertical Half Mirror\n");

	fscanf(stdin,"%d", &userVal);
	/* 
		This while loop checks the user's input to ensure that they selected a valid
		transformation. If they did not, it informs them and allows them to select again.
	*/
	while(userVal < 1 || userVal > 8)
	{
		fprintf(stdout,"That is an invalid selection.\n");
		fprintf(stdout,"Please choose from the list below:\n");
	   fprintf(stdout,"1. Grayscale\n");
	   fprintf(stdout,"2. Color to Negative\n");
	   fprintf(stdout,"3. Flip the Image\n");
	   fprintf(stdout,"4. Rotate Right\n");
		fprintf(stdout,"5. Rotate Left\n");
		fprintf(stdout,"6. Reprint the Image\n");
	   fprintf(stdout,"7. Horizontal Half Mirror\n");
		fprintf(stdout,"8. Vertical Half Mirror\n");
		fscanf(stdin,"%d", &userVal);
	}
	// These if statements call the appropriate function to perform the desired transformation.
	if(userVal == 1)
	{
	grayScaleImage(hdr, pixelArray, saveImg);	
	}
	if(userVal == 3)
	{
	flipImage(hdr, pixelArray, saveImg);
	}
	if(userVal == 5)
	{
	rotateLeft(hdr, pixelArray, saveImg);
	}
	if(userVal == 4)
	{
	rotateRight(hdr, pixelArray, saveImg);
	}
	if(userVal == 2)
	{
	color2Negative(hdr, pixelArray, saveImg);
	}
	if(userVal == 6)
	{
		reprint(hdr, pixelArray, saveImg);
	}
	if(userVal == 7)
	{
		horizontalHalfMirror(hdr, pixelArray, saveImg);
	}
	if(userVal == 8)
	{
		verticalHalfMirror(hdr, pixelArray, saveImg);
	}
}

/*
	This functions prints out a P6 image with
	the header and pixel information provided
	to it in its parameters.
*/
void printP6Image(header_t *hdr, pixel_t **pix, FILE* saveImg)
{
	int i;
	int j;
	fprintf(saveImg, "%s %d %d %d\n", hdr->type, hdr->width, hdr->height, hdr->maxVal);
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
			fprintf(saveImg, "%c%c%c", pix[i][j].red, pix[i][j].green, pix[i][j].blue);
		}
	}
}

/*
	This functions prints out a grayscale
	version of the original image. The grayscale
	image is a P5 image rather than a P6, so it prints
	it out itslef instead of calling the printP6Image functions.
*/
void grayScaleImage(header_t *hdr, pixel_t **arrPix, FILE* saveImg)
{
	int i;
	int j;
	double redVal;
	double greenVal;
	double blueVal;
	/*
		This dynamically allocates memory for the integers which will store the new
		pixel information.
	*/
	int **pixelVals = (int **)malloc(hdr->height * sizeof(int *));
	for(i = 0; i < hdr->height; i++)
	{
		pixelVals[i] = (int *)malloc(hdr->width * sizeof(int));
	}
	// This line prints out the header for the P5 image.
	fprintf(saveImg, "P5 %d %d %d\n", hdr->width, hdr->height, hdr->maxVal);
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{	
			// The following lines calculate the value of the new pixel.
			redVal = ((double)arrPix[i][j].red) * .299;
			greenVal = ((double)arrPix[i][j].green) * .587;
			blueVal = ((double)arrPix[i][j].blue) * .114;
			pixelVals[i][j] = (int)(redVal + greenVal + blueVal);
			// The next line prints out the new pixel value.
			fprintf(saveImg,"%c", pixelVals[i][j]);
		}
	}
	// Now that they are no longer needed, the dynamically allocated memory for the new pixels is freed.
	for(i = 0; i < hdr->height; i++)
	{
	free(pixelVals[i]);
	}
	free(pixelVals);
}

/*
	This functions creates a new 2D array of pixels
	and stores the pixel information in it of the user
	image flipped upside down. It then calls printP6Image
	to print the new image.
*/
void flipImage(header_t *hdr, pixel_t **pixInfo, FILE* saveImg)
{
	int i;
	int j;
	// Dynamically allocates memory for the new pixel information
	pixel_t **newPix = (pixel_t **)malloc(hdr->height * sizeof(pixel_t *));
	for(i = 0; i < hdr->height; i++)
	{
		newPix[i] = (pixel_t *)malloc(hdr->width * sizeof(pixel_t));
	}
	/*
		These nested loops give the new pixels
		the values of the pixels from the original
		image, but feed them from the last row
		to the first row, thus flipping the image.
	*/
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
			newPix[i][j].red = pixInfo[(hdr->height - 1) - i][j].red;
			newPix[i][j].green = pixInfo[(hdr->height - 1) - i][j].green;
			newPix[i][j].blue = pixInfo[(hdr->height - 1) - i][j].blue;
		}
	}
	// Call to printP6Image to printt the new image.
	printP6Image(hdr, newPix, saveImg);
	// Frees the dynamically allocated memory because it has served its purpose.
	for(i = 0; i < hdr->height; i++)
	{
		free(newPix[i]);
	}
	free(newPix);
}

/*
	This function creates a new 2D array of pixels with 
	the same number of rows as the orginal array had columns
	and the same number of columns as the original array had
	rows. It then fills this array with the pixel information
	needed to rotate the image to the left. After that it prints
	the new image by calling printP6Image.
*/
void rotateLeft(header_t *hdr, pixel_t **oldPix, FILE* saveImg)
{
	int i;
	int j;
	int newWidth;
	int newHeight;
	/* Dynamically allocates memory for the new pixels.
		Notice that the number of rows and columns has 
		been flipped from the original array of pixels.
	*/
	pixel_t **newPixels = (pixel_t **)malloc(hdr->width * sizeof(pixel_t *));
	for(i = 0; i < hdr->width; i++)
	{
		newPixels[i] = (pixel_t *)malloc(hdr->height * sizeof(pixel_t));
	}
	/*
		These nested for loops fill the new array with the
		pixel information needed to rotate the user image
		to the left.
	*/
	for(i = 0; i < hdr->width; i++)
	{
		for(j = 0; j < hdr->height; j++)
		{
			newPixels[i][j].red = oldPix[j][(hdr->width - 1) - i].red;
			newPixels[i][j].green = oldPix[j][(hdr->width - 1) - i].green;
			newPixels[i][j].blue = oldPix[j][(hdr->width - 1) - i].blue;
		}
	}
	/*
		As the height and width of the rotated image will be
		swapped relative to the original image, the following
		lines change the header to reflect this new information.
	*/
	newWidth = hdr->height;
	newHeight = hdr->width;
	hdr->width = newWidth;
	hdr->height = newHeight;
	// Prints the image by calling printP6Image.
	printP6Image(hdr, newPixels, saveImg);
	//Frees the dynamically allocated memory for the new pixels.
	for(i = 0; i < hdr->height; i++)
	{
		free(newPixels[i]);
	}
	free(newPixels);
}

/*
   This function creates a new 2D array of pixels with 
   the same number of rows as the orginal array had columns
   and the same number of columns as the original array had
   rows. It then fills this array with the pixel information
   needed to rotate the image to the rught. After that it prints
   the new image by calling printP6Image.
*/
void rotateRight(header_t *hdr, pixel_t **origPix, FILE* saveImg)
{
	int i;
	int j;
	int adjWidth;
	int adjHeight;
	// Dynamically allocates memory for the new 2D array of pixels.	
	pixel_t **rightPix = (pixel_t **)malloc(hdr->width * sizeof(pixel_t *));
	for(i = 0; i < hdr->width; i++)
	{
		rightPix[i] = (pixel_t *)malloc(hdr->height * sizeof(pixel_t));
	}
	/*
		These nested for loops give the new array of pixels
		the values needed to rotate the user image to the 
		right.
	*/
	for(i = 0; i < hdr->width; i++)
	{
		for(j = 0; j < hdr->height; j++)
		{
			rightPix[i][j].red = origPix[(hdr->height - 1) - j][i].red;
			rightPix[i][j].green = origPix[(hdr->height - 1) - j][i].green;
			rightPix[i][j].blue = origPix[(hdr->height - 1) - j][i].blue;
		}
	}
	// The following lines change the header to fit the size of a rotated original image.
	adjWidth = hdr->height;
	adjHeight = hdr->width;
	hdr->width = adjWidth;
	hdr->height = adjHeight;
	//Prints the new image.
	printP6Image(hdr,rightPix,saveImg);
	// Frees the dynamically allocated memory of the new pixel data.
	for(i = 0; i < hdr->height; i++)
	{
		free(rightPix[i]);
	}
	free(rightPix);
}

/*
	The following function changes the pixel
	data of the image to that of the user
	image's negative. It then prints the new
	image by calling printP6Image.
*/
void color2Negative(header_t *hdr, pixel_t **oPix, FILE* saveImg)
{
	int i;
	int j;
	
	//Allocates the memory for the new 2d array of pixels.
	pixel_t **negPix = (pixel_t **)malloc(hdr->height * sizeof(pixel_t *));
	for(i = 0; i < hdr->height; i++)
	{
		negPix[i] = (pixel_t *)malloc(hdr->width * sizeof(pixel_t));
	}

	// These nested loops change the data for each pixel to its negative.
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
			negPix[i][j].red = 255 - oPix[i][j].red;
			negPix[i][j].green = 255 - oPix[i][j].green;
			negPix[i][j].blue = 255 - oPix[i][j].blue;
		}
	}
	// Prints the image
	printP6Image(hdr, negPix, saveImg);
	for(i = 0; i < hdr->height; i++)
	{
		free(negPix[i]);
	}
	free(negPix);
}

/*
	This function calls printP6Image to reprint the user image.
*/
void reprint(header_t *hdr, pixel_t **pixels, FILE* saveImg)
{
	printP6Image(hdr, pixels, saveImg);
}

/*
	This function dynamically allocates memory for a 2D
	array and stores the pixel values of a horizontal half 
	mirror of the user image within it. It then calls printP6Image
	to print the new image.
*/
void horizontalHalfMirror(header_t *hdr, pixel_t **originals, FILE* saveImg)
{
	int i;
	int j;
	//Dynamically allocates memory for the 2d array.
	pixel_t **mirrPix = (pixel_t **)malloc(hdr->height * sizeof(pixel_t *));
	for(i = 0; i < hdr->height; i++)
	{
		mirrPix[i] = (pixel_t *)malloc(hdr->width * sizeof(pixel_t));
	}
	//Fills the new array with the needed pixel values.
	for(i = 0; i < (hdr->height / 2); i++)
	{
		for(j = 0; j < hdr->width; j++)
		{
			mirrPix[i][j].red = originals[i][j].red;
			mirrPix[i][j].green = originals[i][j].green;
			mirrPix[i][j].blue = originals[i][j].blue;
			mirrPix[(hdr->height - 1) -  i][j].red = originals[i][j].red;
			mirrPix[(hdr->height - 1) - i][j].green = originals[i][j].green;
			mirrPix[(hdr->height - 1) - i][j].blue = originals[i][j].blue;
		}
	}
	//Calls printP6Image to print the new image.
	printP6Image(hdr, mirrPix, saveImg);
	//Frees the dynamically allocated memory of the new pixels.
	for(i = 0; i < hdr->height; i++)
	{
		free(mirrPix[i]);
	}
	free(mirrPix);	
}

/*
   This function dynamically allocates memory for a 2D
   array and stores the pixel values of a vertical half 
   mirror of the user image within it. It then calls printP6Image
   to print the new image.
*/
void verticalHalfMirror(header_t *hdr, pixel_t **normPix, FILE* saveImg)
{
	int i;
	int j;
   //Dynamically allocates memory for the 2d array.
	pixel_t **vertPix = (pixel_t **)malloc(hdr->height * sizeof(pixel_t *));
	for(i = 0; i < hdr->height; i++)
	{
		vertPix[i] = (pixel_t *)malloc(hdr->width * sizeof(pixel_t *));
	}
   //Fills the new array with the needed pixel values.
	for(i = 0; i < hdr->height; i++)
	{
		for(j = 0; j < (hdr->width / 2); j++)
		{
			vertPix[i][j].red = normPix[i][j].red;
			vertPix[i][j].green = normPix[i][j].green;
			vertPix[i][j].blue = normPix[i][j].blue;
			vertPix[i][(hdr->width - 1) - j].red = normPix[i][j].red;
			vertPix[i][(hdr->width -1) - j].green = normPix[i][j].green;
			vertPix[i][(hdr->width - 1) - j].blue = normPix[i][j].blue;
		}
	}
   //Calls printP6Image to print the new image.
	printP6Image(hdr, vertPix, saveImg);
   //Frees the dynamically allocated memory of the new pixels.
	for(i = 0; i < hdr->height; i++)
	{
		free(vertPix[i]);
	}
	free(vertPix);
}
