/* Nicholas Muenchen
	CPSC 1020 Section 1, Sp17
	nmuench
*/
#include "functions.h"

int main(int argc, char *argv[])
{
	int k;
	FILE *fp;
	FILE *saveFp;
	/*
	This checks to make sure that there were enough command line arguments.
	If there were not, it informs the user, and the program ends.
	*/
	if(argc < 3)
	{
		fprintf(stdout, "There are not enough command line arguments! Please try again.\n");
		exit(1);
	}
	//Opens the file which contains the image that will be transformed.
	fp = fopen(argv[1], "r");
	/*
		This checks to ensure that the image file opened successfully. 
		If it failed to open correctly, then the program informs the user of the error and ends.
	*/
	if(fp == NULL)
	{
		fprintf(stdout,"The file containing the image is invalid, please try again\n");
		exit(1);
	}
	// Opens the file into which the transformed image is to be printed.
	saveFp = fopen(argv[2], "w");
	/*
		This checks to ensure that the save file has been opened correctly.
		If the file was unable to be opened, then it informs the user and
		terminates the program.
	*/
	if(saveFp == NULL)
	{
		fprintf(stdout,"The file designated for saving the new image can't be opened. Please try again\n");
		exit(1);
	}
	
	header_t hdr;
	readHeader(&hdr,fp);
	/*
		The following lines dynamically allocate in which the pixels of the image 
		are to be stored.
	*/
	pixel_t **arrayOfPixels = (pixel_t **)malloc(hdr.height * sizeof(pixel_t *));
	for(k = 0; k < hdr.height; k++)
	{
		arrayOfPixels[k] = (pixel_t *)malloc(hdr.width * sizeof(pixel_t));
	}
	// Now that the memory has been allocated, the pixels are read into the array.
	readImage(&hdr,arrayOfPixels,fp);
	// With the header and pixels read in, the program is ready to perform the transformations.
	chooseTransform(&hdr, arrayOfPixels, saveFp);
	//Restores the original header values so that it can be used to free original pixels
	readHeader(&hdr, fp);
	//The files are no longer needed, so they are now closed.
	fclose(fp);
	fclose(saveFp);
	//The memory allocated for the pixels is no longer needed, so it is freed.
	for(k = 0; k < hdr.height; k++)
	{
		free(arrayOfPixels[k]);
	}
	free(arrayOfPixels);
	
	return 0;
}
